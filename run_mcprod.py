#!/usr/bin/env cctools_python
# CCTOOLS_PYTHON_VERSION 2.7 2.6

# Copyright (c) 2010- The University of Notre Dame.
# This software is distributed under the GNU General Public License.
# See the file COPYING for details.
import os, sys, random, time
import shutil
import json

from os.path import expanduser
HOME = expanduser("~")

from prune import client

base_dir = '/data/prune_space.mcprod'
# shutil.rmtree(base_dir)
prune = client.Connect(base_dir = base_dir)


###### Some files used in the workflow are no longer available. Please see the Prune manual for a new workflow that used fixed replacement files.


###### Load the preserved workflow ######
prune.load('mcprod.export.prune')

###### Execute the workflow ######
prune.execute( worker_type='local', cores=8 )

