#!/usr/bin/env cctools_python
# CCTOOLS_PYTHON_VERSION 2.7 2.6

# Copyright (c) 2010- The University of Notre Dame.
# This software is distributed under the GNU General Public License.
# See the file COPYING for details.
import os, sys, random, time
import shutil
import json

from os.path import expanduser
HOME = expanduser("~")

from prune import client

base_dir = '/data/pivie/prune_space.merge_sort'
# shutil.rmtree(base_dir)
prune = client.Connect(base_dir = base_dir)



###### Load the preserved workflow ######
prune.load('merge_sort.prune')

###### Execute the workflow ######
prune.execute( worker_type='local', cores=8 )

###### Export final data ######
prune.export( 'e82855394e9dcdee03ed8a25c96c79245fd0481a:0', 'merged_words.txt' )

