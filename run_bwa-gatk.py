#!/usr/bin/env cctools_python
# CCTOOLS_PYTHON_VERSION 2.7 2.6

# Copyright (c) 2010- The University of Notre Dame.
# This software is distributed under the GNU General Public License.
# See the file COPYING for details.
import os, sys, random, time
import shutil
import json

from os.path import expanduser
HOME = expanduser("~")

from prune import client

base_dir = '/data/prune_space.bwa-gatk'
# shutil.rmtree(base_dir)
prune = client.Connect(base_dir = base_dir)



###### Restricted files that must be obtained manually ######
domain_directory = HOME
sortsam = prune.file_add(domain_directory+'SortSam.jar')
addorreplacereadgroups = prune.file_add(domain_directory+'AddOrReplaceReadGroups.jar')
buildbamindex = prune.file_add(domain_directory+'BuildBamIndex.jar')
createsequencedictionary = prune.file_add(domain_directory+'CreateSequenceDictionary.jar')
genomeanalysistk = prune.file_add(domain_directory+'GenomeAnalysisTK.jar')



###### Load the preserved workflow ######
prune.load('bwa-gatk.export.prune')

###### Execute the workflow ######
prune.execute( worker_type='local', cores=8 )

