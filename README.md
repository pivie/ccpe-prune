## Welcome to the preserved archive of workflows in the paper published in Concurrency & Computation: Practice & Experience entitled "Using PRUNE to reproducibly manage the data requirements in computational science."

### After [installing cctools](https://ccl.cse.nd.edu/software/manuals/install.html), run the following command to reproduce a merge_sort workflow sample like this:
```
./run_merge_sort.py
```

### The other workflows can be run similarly, however, restricted files need to be obtained and downloaded before this is possible.

#### Also note that with the passage of time, the mcprod workflow needed to be updated in order to accomodate for the behavior of temporary files that were being stored remotely rather than locally. Please find the updated workflow on the [webpage for the PRUNE manual](https://ccl.cse.nd.edu/software/manuals/prune.html).
