#!/usr/bin/env cctools_python
# CCTOOLS_PYTHON_VERSION 2.7 2.6

# Copyright (c) 2010- The University of Notre Dame.
# This software is distributed under the GNU General Public License.
# See the file COPYING for details.
import os, sys, random, time
import shutil
import json

from os.path import expanduser
HOME = expanduser("~")

from prune import client

base_dir = '/data/prune_space.census'
# shutil.rmtree(base_dir)
prune = client.Connect(base_dir = base_dir)



###### Restricted files that must be obtained manually ######
years = [1850, 1860, 1870, 1880, 1890, 1900, 1910, 1920, 1930, 1940]
data_folder = HOME  #'/data/census_data.original.zipped/'
for year in years:
        if year == 1870:
                waypoints = prune.file_add(data_folder + '1870_way.gz')

        folder = '%s%i/'%(data_folder,year)
        for (dirpath, dirnames, filenames) in os.walk(folder):
                for u,filename in enumerate(filenames):
                        target = prune.file_add(folder + filename)


###### Load the preserved workflow ######
prune.load('census.export.prune')

###### Execute the workflow ######
prune.execute( worker_type='local', cores=8 )

